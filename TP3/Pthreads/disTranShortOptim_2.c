#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <omp.h>

#define MAX 0xFFFF
int numProc;
pthread_barrier_t barrier;

int height;
int width;

typedef struct arg
{
    unsigned short *c;
    unsigned short *r;
    long *numlines;
    int *above;
    int *bellow;
    long *displacement;
    long *tamanhos;
} Args;

Args args;

unsigned transform(long height, int width, unsigned short fst[height][width], unsigned short snd[height][width], int above, int bellow)
{
    unsigned linebrancos = 0;
    unsigned brancos = 0;

    printf("above = %d bellow = %d\n", above, bellow);
    //Mudados os limites nesta linha, para não processar linhas partilhadas
    for (int i = above; i < bellow; i++)
    {
        linebrancos = 0;
        if (fst[i][0])
        {
            //alterada esta linha pq primeiro elemento do array contem o numero de brancos na linha
            for (int j = 1; j < width; j++)
            {
                if (fst[i][j] == MAX)
                {
                    unsigned short min = MAX;
                    for (int y = -1; y < 2; y++)
                    {
                        for (int x = -1; x < 2; x++)
                        {
                            //Alterados os limites no processamento
                            if (i + y >= 0 && i + y < height && j + x >= 1 && j + x < width && !(y == 0 && x == 0))
                            {
                                unsigned short aux = fst[i + y][j + x];
                                if (aux < min)
                                    min = aux;
                            }
                        }
                    }
                    if (min >= MAX - 1)
                    {
                        snd[i][j] = MAX;
                        linebrancos++;
                    }
                    else
                    {
                        snd[i][j] = min + 1;
                    }
                }
                else
                    snd[i][j] = fst[i][j];
            }
            // Total de brancos guardada na primeira coluna da matriz
            snd[i][0] = linebrancos;
        }
        //Se a linha fst nao tiver brancos e a snd for diferente copia o array
        if (!fst[i][0] && snd[i][0])
        {
            memcpy(snd[i], fst[i], width * sizeof(unsigned short int));
        }
        brancos += linebrancos;
    }
    return brancos;
}

void *distTrans(void *rank)
{
    long th = (long)rank;
    unsigned brancos = 1;
    long lines = args.tamanhos[th];

    unsigned short int *aux;

    long start = args.displacement[th];
    long end = start + lines;

    printf("Thread %ld displacement = %ld tamanho = %ld processadas = %ld\n", th, start, lines, end);

    while (brancos)
    {
        brancos = transform(lines, width, (short unsigned int(*)[width])args.c, (short unsigned int(*)[width])args.r, start, end);
        pthread_barrier_wait(&barrier);
        aux = (unsigned short int *)args.c;
        args.c = args.r;
        args.r = aux;
    }
    return NULL;
}

//________________________________________________________________

void save_pgm(unsigned short int *m)
{
    FILE *pgmimg = fopen("new.pgm", "wb");
    // Writing Magic Number to the File
    fprintf(pgmimg, "P2\n");
    // Writing Width and Height
    fprintf(pgmimg, "%u %u\n", width - 1, height);
    // Writing the maximum gray value

    unsigned short int max_grey = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            if (m[i * width + j] >= max_grey)
                max_grey = m[i * width + j];
        }
    }
    fprintf(pgmimg, "%u\n", max_grey);
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            fprintf(pgmimg, "%u ", m[i * width + j]);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
}

unsigned char magic_num[2];
unsigned gray_value = 0;
int brancostotal = 0;
unsigned short int *load_img(const char *filename)
{
    FILE *pgmimg = fopen(filename, "r");
    fseek(pgmimg, -1, SEEK_CUR);
    fscanf(pgmimg, "%s", magic_num);
    while (getc(pgmimg) == '#')
    { /* skip comment lines */
        while (getc(pgmimg) != '\n')
            ; /* skip to end of comment line */
    }
    fscanf(pgmimg, "%u", &width);
    fscanf(pgmimg, "%u", &height);
    fscanf(pgmimg, "%u", &gray_value);
    //Primeira posição do array é o numero de brancos
    width++;

    unsigned short int *img = (unsigned short int *)malloc(sizeof(unsigned short int) * height * width);
    int x;

    for (int i = 0; i < height; i++)
    {
        int brancosInLine = 0;
        for (int j = 1; j < width; j++)
        {
            fscanf(pgmimg, "%u", &x);
            if (x)
            {
                brancosInLine++;
                x = MAX;
            }
            img[i * width + j] = x;
            img[i * width] = brancosInLine;
        }
        brancostotal += brancosInLine;
    }
    fclose(pgmimg);
    return img;
}
//________________________________________________________________

void builddispMatrix(unsigned height, unsigned width, unsigned short int img[height][width], long displacement[numProc], long tamanhos[numProc])
{
    int tbrancos = height + brancostotal;
    int fraction = tbrancos / numProc;
    unsigned numLines = 0;
    unsigned currThread = 0;
    unsigned brancosThread = 0;
    int procRemaining = numProc;

    int disp = 0;
    for (int i = 0; i < height - 2; i++)
    {
        int numBrancos = img[i][0] + 1;
        int nextaux = img[i + 1][0] + 1;
        numLines++;
        brancosThread += numBrancos;
        if (fraction >= brancosThread && fraction < (brancosThread + nextaux))
        {
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            procRemaining--;
            tbrancos -= brancosThread;
            fraction = tbrancos / procRemaining;
            currThread++;
            disp += numLines;
            brancosThread = 0;
            numLines = 0;
        }
        if (procRemaining == 1)
        {
            numLines = height - i - 1;
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            i = height;
        }
    }
}

int main(int argc, char const *argv[])
{
    if (argc >= 2 && argc <= 3)
    {
        if (argc == 3)
            thread_count = atoi(argv[2]);
        printf("Threads = %d\n", thread_count);
        args.c = load_img(argv[1]);
        printf("Img = %s\nHeight = %d Width = %d\n", argv[1], height, width - 1);

        args.above = malloc(numProc * sizeof(int));
        args.bellow = malloc(numProc * sizeof(int));
        args.numlines = malloc(numProc * sizeof(long));

        for (int i = 0; i < numProc; i++)
        {
            if (i > 0)
                args.above[i] = i;
            if (i < numProc - 1)
                args.bellow[i] = i < numProc - 1;
        }

        //Array dos displacements
        args.displacement = malloc(numProc * sizeof(long));
        //Array dos tamanhos atríbuidos a cada thread
        args.tamanhos = malloc(numProc * sizeof(long));

        builddispMatrix(height, width, (short unsigned int(*)[width])args.c, args.displacement, args.tamanhos);

        long thread;
        pthread_t *thread_handles;
        thread_handles = malloc(numProc * sizeof(pthread_t));

        pthread_barrier_init(&barrier, NULL, numProc);

        double time = omp_get_wtime();
        for (thread = 0; thread < numProc; thread++)
        {
            pthread_create(&thread_handles[thread], (pthread_attr_t *)NULL, distTrans, (void *)thread);
        }
        for (thread = 0; thread < numProc; thread++)
        {
            pthread_join(thread_handles[thread], NULL);
        }
        printf("Time = %f\n", omp_get_wtime() - time);

        save_pgm(args.c);
    }

    return 0;
}
