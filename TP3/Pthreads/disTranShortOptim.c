#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <omp.h>
#include "pthread_barrier.c"

#define MAX 0xFFFF
int numProc = 1;
pthread_barrier_t barrier;
pthread_mutex_t mutex;

int height;
int width;
unsigned short *c;
unsigned short *r;
long *displacement;
long *tamanhos;

unsigned transform(long height, int width, unsigned short fst[height][width], unsigned short snd[height][width], int above, int bellow)
{
    unsigned linebrancos = 0;
    unsigned brancos = 0;

    //printf("above = %d bellow = %d\n", above, bellow);
    //Mudados os limites nesta linha, para não processar linhas partilhadas
    for (int i = above; i < bellow; i++)
    {
        //printf("-------------I %d \n", i);
        linebrancos = 0;
        if (fst[i][0])
        {
            //alterada esta linha pq primeiro elemento do array contem o numero de brancos na linha
            for (int j = 1; j < width; j++)
            {
                //printf("-------------J %d \n", j);
                if (fst[i][j] == MAX)
                {
                    unsigned short min = MAX;
                    for (int y = -1; y < 2; y++)
                    {
                        for (int x = -1; x < 2; x++)
                        {
                            //Alterados os limites no processamento
                            if (i + y >= 0 && i + y < height && j + x >= 1 && j + x < width && !(y == 0 && x == 0))
                            {
                                unsigned short aux = fst[i + y][j + x];
                                if (aux < min)
                                    min = aux;
                            }
                        }
                    }
                    if (min >= MAX - 1)
                    {
                        snd[i][j] = MAX;
                        linebrancos++;
                    }
                    else
                    {
                        snd[i][j] = min + 1;
                    }
                }
                else
                    snd[i][j] = fst[i][j];
            }
            // Total de brancos guardada na primeira coluna da matriz
            snd[i][0] = linebrancos;
        }
        //Se a linha fst nao tiver brancos e a snd for diferente copia o array
        if (!fst[i][0] && snd[i][0])
        {
            memcpy(snd[i], fst[i], width * sizeof(unsigned short int));
        }
        brancos += linebrancos;
    }
    return brancos;
}

int brancosGlobal = 1;
void *distTrans(void *rank)
{
    long th = (long)rank;
    unsigned brancos = 1;
    long lines = tamanhos[th];
    long start = displacement[th];
    long end = start + lines;

    unsigned short int *aux;
    unsigned short int *cc = c;
    unsigned short int *rr = r;

    //printf("Thread %ld displacement = %ld tamanho = %ld processadas = %ld\n", th, start, lines, end);

    int brancosOld = 0;
    while (brancosGlobal > brancosOld)
    {
        brancosOld = brancosGlobal;
        pthread_barrier_wait(&barrier);

        brancos = transform(height, width, (short unsigned int(*)[width])cc, (short unsigned int(*)[width])rr, start, end);

        pthread_mutex_lock(&mutex);
        brancosGlobal += brancos;
        pthread_mutex_unlock(&mutex);

        //printf("------------- %ld \n", th);
        pthread_barrier_wait(&barrier);
        aux = (unsigned short int *)cc;
        cc = rr;
        rr = aux;
    }
    if (th == 0)
    {
        c = cc;
        r = rr;
    }

    return NULL;
}

//________________________________________________________________

void save_pgm(unsigned short int *m)
{
    FILE *pgmimg = fopen("new.pgm", "wb");
    // Writing Magic Number to the File
    fprintf(pgmimg, "P2\n");
    // Writing Width and Height
    fprintf(pgmimg, "%u %u\n", width - 1, height);
    // Writing the maximum gray value

    unsigned short int max_grey = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            if (m[i * width + j] >= max_grey)
                max_grey = m[i * width + j];
        }
    }
    fprintf(pgmimg, "%u\n", max_grey);
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            fprintf(pgmimg, "%u ", m[i * width + j]);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
}

unsigned char magic_num[2];
unsigned gray_value = 0;
int brancostotal = 0;
unsigned short int *load_img(const char *filename)
{
    FILE *pgmimg = fopen(filename, "r");
    fseek(pgmimg, -1, SEEK_CUR);
    fscanf(pgmimg, "%s", magic_num);
    while (getc(pgmimg) == '#')
    { /* skip comment lines */
        while (getc(pgmimg) != '\n')
            ; /* skip to end of comment line */
    }
    fscanf(pgmimg, "%u", &width);
    fscanf(pgmimg, "%u", &height);
    fscanf(pgmimg, "%u", &gray_value);
    //Primeira posição do array é o numero de brancos
    width++;

    unsigned short int *img = (unsigned short int *)malloc(sizeof(unsigned short int) * height * width);
    int x;

    for (int i = 0; i < height; i++)
    {
        int brancosInLine = 0;
        for (int j = 1; j < width; j++)
        {
            fscanf(pgmimg, "%u", &x);
            if (x)
            {
                brancosInLine++;
                x = MAX;
            }
            img[i * width + j] = x;
            img[i * width] = brancosInLine;
        }
        brancostotal += brancosInLine;
    }
    fclose(pgmimg);
    return img;
}
//________________________________________________________________

void builddispMatrix(unsigned height, unsigned width, unsigned short int img[height][width], long displacement[numProc], long tamanhos[numProc])
{
    int tbrancos = height + brancostotal;
    int fraction = tbrancos / numProc;
    unsigned numLines = 0;
    unsigned currThread = 0;
    unsigned brancosThread = 0;
    int procRemaining = numProc;

    int disp = 0;
    for (int i = 0; i < height - 2; i++)
    {
        int numBrancos = img[i][0] + 1;
        int nextaux = img[i + 1][0] + 1;
        numLines++;
        brancosThread += numBrancos;
        if (fraction >= brancosThread && fraction < (brancosThread + nextaux))
        {
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            procRemaining--;
            tbrancos -= brancosThread;
            fraction = tbrancos / procRemaining;
            currThread++;
            disp += numLines;
            brancosThread = 0;
            numLines = 0;
        }
        if (procRemaining == 1)
        {
            numLines = height - i - 1;
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            i = height;
        }
    }
}

int main(int argc, char const *argv[])
{
    if (argc >= 2 && argc <= 3)
    {
        if (argc == 3)
            numProc = atoi(argv[2]);
        //printf("Threads = %d\n", numProc);

        c = load_img(argv[1]);
        r = (unsigned short int *)malloc(sizeof(unsigned short int) * height * width);
        //printf("Img = %s\nHeight = %d Width = %d\n", argv[1], height, width - 1);

        //Array dos displacements
        displacement = malloc(numProc * sizeof(long));
        //Array dos tamanhos atríbuidos a cada thread
        tamanhos = malloc(numProc * sizeof(long));

        builddispMatrix(height, width, (short unsigned int(*)[width])c, displacement, tamanhos);

        long thread;
        pthread_t *thread_handles;
        thread_handles = malloc(numProc * sizeof(pthread_t));

        pthread_barrier_init(&barrier, NULL, numProc);
        pthread_mutex_init(&mutex, NULL);

        //double time = omp_get_wtime();
        for (thread = 0; thread < numProc; thread++)
        {
            pthread_create(&thread_handles[thread], (pthread_attr_t *)NULL, distTrans, (void *)thread);
        }
        for (thread = 0; thread < numProc; thread++)
        {
            pthread_join(thread_handles[thread], NULL);
        }
        pthread_barrier_destroy(&barrier);
        pthread_mutex_destroy(&mutex);
        //printf("Time = %f\n", omp_get_wtime() - time);

        save_pgm(c);

        free(c);
        free(r);
        free(displacement);
        free(tamanhos);
    }

    return 0;
}
