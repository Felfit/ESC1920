#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "pthread_barrier.c"

FILE *pgmimg;
unsigned char magic_num[2];
unsigned gray_value = 0;
#define MAX 0xFFFF

int thread_count = 1;

int brancostotal = 0;

pthread_mutex_t mutex;
pthread_barrier_t barrier;

int height;
int width;
unsigned short **c;
unsigned short **r;
int *brancosT;
long *displacement;
long *tamanhos;

void builddispMatrix()
{
    int tbrancos = height + brancostotal;
    int fraction = tbrancos / thread_count;
    unsigned numLines = 0;
    unsigned currThread = 0;
    unsigned brancosThread = 0;
    int procRemaining = thread_count;

    int disp = 0;
    for (int i = 0; i < height - 2; i++)
    {
        int numBrancos = c[i][0] + 1;
        int nextaux = c[i + 1][0] + 1;
        numLines++;
        brancosThread += numBrancos;
        if (fraction >= brancosThread && fraction < (brancosThread + nextaux))
        {
            brancosT[currThread] = brancosThread;
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            procRemaining--;
            tbrancos -= brancosThread;
            fraction = tbrancos / procRemaining;
            currThread++;
            disp += numLines;
            brancosThread = 0;
            numLines = 0;
        }
        if (procRemaining == 1)
        {
            brancosT[currThread] = tbrancos - height;
            numLines = height - i - 1;
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            i = height;
        }
    }
}

void *transform(void *rank)
{
    long thread = (long)rank;

    brancosT[thread] = 0;

    int lines = tamanhos[thread];
    int start = displacement[thread];
    int end = start + lines;

    //pthread_mutex_lock(&mutex);
    //printf("THREAD = %ld Start = %d End = %d Lines = %d\n", thread, start, end, lines);
    //for (int i = start; i < end; i++)
    //{
    //    for (int j = 0; j < width; j++)
    //    {
    //        printf("%u", c[i][j]);
    //    }
    //    printf("\n");
    //}
    //pthread_mutex_unlock(&mutex);

    //printf("Thread %ld displacement = %d tamanho = %d processadas = %d\n", thread, start, lines, end);
    for (long i = start; i < end; i++)
    {
        unsigned linebrancos = 0;
        if (c[i][0])
        {
            for (int j = 1; j < width; j++)
            {
                if (c[i][j] == MAX)
                {
                    unsigned short min = MAX;
                    for (int y = -1; y < 2; y++)
                    {
                        for (int x = -1; x < 2; x++)
                        {
                            if (i + y >= 0 && i + y < height && j + x >= 1 && j + x < width && !(y == 0 && x == 0))
                            {
                                unsigned short aux = c[i + y][j + x];
                                if (aux < min)
                                    min = aux;
                            }
                        }
                    }
                    if (min >= MAX - 1)
                    {
                        r[i][j] = MAX;
                        linebrancos++;
                    }
                    else
                        r[i][j] = min + 1;
                }
                else
                    r[i][j] = c[i][j];
            }
            r[i][0] = linebrancos;
        }
        //Se a linha c nao tiver brancos e a r for diferente copia o array
        if (!c[i][0] && r[i][0])
        {
            memcpy(r[i], c[i], width * sizeof(unsigned short int));
        }
        brancosT[thread] += linebrancos;
    }
    return NULL;
}

int distTrans()
{
    long thread;
    pthread_t *thread_handles;
    thread_handles = malloc(thread_count * sizeof(pthread_t));

    pthread_mutex_init(&mutex, NULL);

    int brancos = 1;
    brancosT = malloc(thread_count * sizeof(int));
    //Array dos displacements
    displacement = malloc(thread_count * sizeof(long));
    //Array dos tamanhos atríbuidos a cada thread
    tamanhos = malloc(thread_count * sizeof(long));

    builddispMatrix();

    //printf("Brancos = %d\n", brancostotal);
    //int bs[thread_count];
    while (brancos)
    {
        brancos = 0;
        for (thread = 0; thread < thread_count; thread++)
        {
            //bs[thread] = brancosT[thread];
            pthread_create(&thread_handles[thread], (pthread_attr_t *)NULL, transform, (void *)thread);
        }
        for (thread = 0; thread < thread_count; thread++)
        {
            pthread_join(thread_handles[thread], NULL);
            pthread_mutex_lock(&mutex);
            brancos += brancosT[thread];
            pthread_mutex_unlock(&mutex);
            //printf("TH = %ld B_Antes = %d B_depois = %d tot = %d\n", thread, bs[thread], brancosT[thread], brancos);
        }
        unsigned short **aux = c;
        c = r;
        r = aux;
    }
    free(displacement);
    free(tamanhos);
    free(thread_handles);
    pthread_mutex_destroy(&mutex);
    return 1;
}

//________________________________________________________________

void load_img(char *filename)
{
    FILE *pgmimg = fopen(filename, "r");
    fseek(pgmimg, -1, SEEK_CUR);
    fscanf(pgmimg, "%s", magic_num);
    while (getc(pgmimg) == '#')
    { /* skip comment lines */
        while (getc(pgmimg) != '\n')
            ; /* skip to end of comment line */
    }
    fscanf(pgmimg, "%u", &width);
    fscanf(pgmimg, "%u", &height);
    fscanf(pgmimg, "%u", &gray_value);
    //Primeira posição do array é o numero de brancos
    width++;
    c = (unsigned short **)malloc(height * sizeof(unsigned short *));
    r = (unsigned short **)malloc(height * sizeof(unsigned short *));
    unsigned x;
    for (int i = 0; i < height; i++)
    {
        c[i] = (unsigned short *)malloc(width * sizeof(unsigned short));
        r[i] = (unsigned short *)malloc(width * sizeof(unsigned short));
        int brancosInLine = 0;
        for (int j = 1; j < width; j++)
        {
            fscanf(pgmimg, "%u", &x);
            if (x)
            {
                x = MAX;
                brancosInLine++;
            }
            c[i][0] = brancosInLine;
            c[i][j] = x;
        }
        brancostotal += brancosInLine;
    }
    fclose(pgmimg);
}

void save_pgm(unsigned short **m)
{
    FILE *pgmimg = fopen("new.pgm", "wb");
    // Writing Magic Number to the File
    fprintf(pgmimg, "P2\n");
    // Writing Width and Height
    fprintf(pgmimg, "%u %u\n", width - 1, height);
    // Writing the maximum gray value
    unsigned short max_grey = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            if (m[i][j] >= max_grey)
                max_grey = m[i][j];
        }
    }
    fprintf(pgmimg, "%u\n", max_grey);
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            fprintf(pgmimg, "%u ", m[i][j]);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
}

//________________________________________________________________

int main(int argc, char **argv)
{
    if (argc >= 2 && argc <= 3)
    {
        if (argc == 3)
            thread_count = atoi(argv[2]);
        printf("Threads = %d\n", thread_count);

        load_img(argv[1]);
        printf("Img = %s\nHeight = %d Width = %d\n", argv[1], height, width - 1);

        double time = omp_get_wtime();
        int x = distTrans();
        printf("Time = %f\n", omp_get_wtime() - time);

        if (x == 1)
        {
            save_pgm(c);
        }
        free(c);
        free(r);
    }
    else
    {
        printf("No input!!!\n");
    }
    return 0;
}
