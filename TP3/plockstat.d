
plockstat$target:::mutex-block
/ execname == "disTranShortOpti"/
{
    block[pid] = timestamp;
}

plockstat$target:::mutex-spin
/ execname == "disTranShortOpti"/
{
    spin[pid] = timestamp;
}

plockstat$target:::mutex-acquire
/ execname == "disTranShortOpti" && block[pid] != 0 && spin[pid] != 0/
{
    @acbl["mutex wait"] = quantize(timestamp - block[pid]);
    block[pid] = 0;
    @acsp["mutex wait"] = quantize(timestamp - spin[pid]);
    spin[pid] = 0;
    acquire[pid] = timestamp;
}

plockstat$target:::mutex-release
/ execname == "disTranShortOpti" && acquire[pid] != 0 /
{
    @acq["mutex time"] = quantize(timestamp - acquire[pid]);
    acquire[pid] = 0;
}

plockstat$target:::mutex-error
/ execname == "disTranShortOpti"/
{
    block[pid] = 0;
    spin[pid] = 0;
    acquire[pid] = 0;
    @error[pid] = count();
}

END
{
	printf("mutex block:\n");
	printa(@acbl);
    printf("mutex spin:\n");
	printa(@acsp);
    printf("mutex time:\n");
	printa(@acq);
	printf("mutex errors:\n");
	printa("%3d %@d\n", @error);
    clear(@acq);
    clear(@error);
}
