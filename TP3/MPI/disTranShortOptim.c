
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "dmpi.h"

#define MAX 0xFFFF
int rank = 0;
int numProc;
int height;
int width;

#ifdef TRACK
unsigned brancosConvertidos = 0;
unsigned iteracoes = 0;
unsigned linesSkipped = 0;
char filename[1024];
FILE *f;
#endif
unsigned transformMPI(int height, int width, unsigned short fst[height][width], unsigned short snd[height][width], int above, int bellow)
{
    unsigned linebrancos = 0;
    unsigned brancos = 0;
    int lastline = height - bellow;
    //int brancosprocessados = 0;
    //Mudados os limites nesta linha, para não processar linhas partilhadas
    for (int i = above; i < lastline; i++)
    {
        linebrancos = 0;
        //TODO: Adicionar condição para saltar linhas que só tem brancos, speedup gigante no gradiente
        if (fst[i][0])
        {
            //alterada esta linha pq primeiro elemento do array contem o numero de brancos na linha
            for (int j = 1; j < width; j++)
            {
                if (fst[i][j] == MAX)
                {
                    unsigned short min = MAX;
                    for (int y = -1; y < 2; y++)
                    {
                        for (int x = -1; x < 2; x++)
                        {
                            //Alterados os limites no processamento
                            if (i + y >= 0 && i + y < height && j + x >= 1 && j + x < width && !(y == 0 && x == 0))
                            {
                                unsigned short aux = fst[i + y][j + x];
                                if (aux < min)
                                    min = aux;
                            }
                        }
                    }
                    if (min >= MAX - 1)
                    {
                        snd[i][j] = MAX;
                        linebrancos++;
                    }
                    else
                    {
                        snd[i][j] = min + 1;
#ifdef TRACK
                        brancosConvertidos++;
#endif
                    }
                    //brancosprocessados++;
                }
                else
                    snd[i][j] = fst[i][j];
            }
            // Total de brancos guardada na primeira coluna da matriz
            snd[i][0] = linebrancos;
            
            if (MPI_QUERY_PROCESSLINE_ENABLED())
                MPI_QUERY_PROCESSLINE(rank,linebrancos);
        }
#ifdef TRACK
        else
        {
            linesSkipped++;
        }
#endif
        //Se a linha fst nao tiver brancos e a snd for diferente copia o array
        if (!fst[i][0] && snd[i][0])
        {
            memcpy(snd[i], fst[i], width * sizeof(unsigned short int));
        }
        brancos += linebrancos;
    }
    return brancos;
}

/*
    Formato de cada bloco
    Array:
    [
        [1ª linha partilhada com o bloco de cima];
        [...],[...]
        [Ultima linha partilhada com o bloco de baixo];
    ]
    Formato de uma linha:
    [1º elemento numero de brancos, resto dos elementos imagem]
*/
unsigned short int *distTransMPI(int numlines, int above, int bellow,
                                 int width,
                                 unsigned short int fst[numlines][width],
                                 unsigned short int snd[numlines][width])
{
    char hasBrancos[height];
    unsigned brancos = 1;
    unsigned short int *aux;
    int recieveTop = above;
    int recieveBottom = bellow;

//-------------------------------------------
#ifdef DEBUG
    int numIts = 0;
#endif
    //Itera--------------------------------------
    MPI_Request sendTop;
    MPI_Request sendBot;
    MPI_Request recTop;
    MPI_Request recBot;
   
    if (MPI_QUERY_DISTTRANSSTART_ENABLED())
                MPI_QUERY_DISTTRANSSTART(rank,numlines);

    while (brancos)
    {
        //Processamento
        if (above && recieveTop)
        {
            MPI_Irecv(fst[0], width, MPI_UNSIGNED_SHORT, rank - 1, 0, MPI_COMM_WORLD, &recTop);
            MPI_Isend(fst[1], width, MPI_UNSIGNED_SHORT, rank - 1, 0, MPI_COMM_WORLD, &sendTop);
            if (MPI_QUERY_SENDMESSAGE_ENABLED())
                        MPI_QUERY_SENDMESSAGE(rank);
        }
        if (bellow && recieveBottom)
        {
            MPI_Irecv(fst[numlines - 1], width, MPI_UNSIGNED_SHORT, rank + 1, 0, MPI_COMM_WORLD, &recBot);
            MPI_Isend(fst[numlines - 2], width, MPI_UNSIGNED_SHORT, rank + 1, 0, MPI_COMM_WORLD, &sendBot);
            if (MPI_QUERY_SENDMESSAGE_ENABLED())
                        MPI_QUERY_SENDMESSAGE(rank);
        }

        if (bellow && recieveBottom)
        {
            MPI_Wait(&recBot, NULL);
            MPI_Wait(&sendBot, NULL);
            if (fst[numlines - 1][0] == 0)
            {
                memcpy(snd, fst[numlines - 1], width * sizeof(unsigned short int));
                recieveBottom = 0;
            }
            if (fst[numlines - 2][0] == 0)
            {
                recieveBottom = 0;
            }
            if (MPI_QUERY_RECIEVEMESSAGE_ENABLED())
                        MPI_QUERY_RECIEVEMESSAGE(rank);
#ifdef DEBUG
            printf("It %d, R %d : Rec Bellow from %d-%d brancos", numIts, rank, rank + 1, fst[numlines - 1][0]);
#endif
        }
        //Verifica se recebeu
        if (above && recieveTop)
        {
            MPI_Wait(&recTop, NULL);
            MPI_Wait(&sendTop, NULL);
            if (fst[0][0] == 0)
            {
                memcpy(snd, fst, width * sizeof(unsigned short int));
                recieveTop = 0;
            }
            if (fst[1][0] == 0)
            {
                recieveTop = 0;
            }
            if (MPI_QUERY_RECIEVEMESSAGE_ENABLED())
                        MPI_QUERY_RECIEVEMESSAGE(rank);
#ifdef DEBUG
            printf("It %d, R %d : Rec Above from %d-%d brancos", numIts, rank, rank - 1, fst[0][0]);
#endif
        }

        if (MPI_QUERY_STARTTRANSFORM_ENABLED())
                        MPI_QUERY_STARTTRANSFORM(rank,brancos);

        brancos = transformMPI(numlines, width, fst, snd, above, bellow);
        
        if (MPI_QUERY_ENDTRANSFORM_ENABLED())
                        MPI_QUERY_ENDTRANSFORM(rank,brancos);

// Verifica se recebeu mensagem do escalonador para futuro escalonamento

//ENVIAR COISAS
#ifdef TRACK
        fprintf(f, "Rank %d| iteração %d| Sent (up,down) (%d,%d)| Brancos Processados %d| Linhas Saltadas %d| Linhas Processadas %d| Width %d \n",
                rank,
                iteracoes,
                recieveTop,
                recieveBottom,
                brancos + brancosConvertidos,
                linesSkipped,
                numlines - bellow - above,
                width - 1);
        iteracoes++;
        brancosConvertidos = 0;
        linesSkipped = 0;
#endif
        aux = (unsigned short int *)fst;
        fst = snd;
        snd = (short unsigned int(*)[])aux;
    }
    //Enviar coisas se saiu para nao bloquear os colegas
    if (above && recieveTop)
    {
        
        MPI_Irecv(fst[0], width, MPI_UNSIGNED_SHORT, rank - 1, 0, MPI_COMM_WORLD, &recTop);
        MPI_Isend(fst[1], width, MPI_UNSIGNED_SHORT, rank - 1, 0, MPI_COMM_WORLD, &sendTop);
        if (MPI_QUERY_SENDMESSAGE_ENABLED())
                        MPI_QUERY_SENDMESSAGE(rank);
    }
    if (bellow && recieveBottom)
    {
        
        MPI_Irecv(fst[numlines - 1], width, MPI_UNSIGNED_SHORT, rank + 1, 0, MPI_COMM_WORLD, &recBot);
        MPI_Isend(fst[numlines - 2], width, MPI_UNSIGNED_SHORT, rank + 1, 0, MPI_COMM_WORLD, &sendBot);
        if (MPI_QUERY_SENDMESSAGE_ENABLED())
                        MPI_QUERY_SENDMESSAGE(rank);
    }

    
    if (MPI_QUERY_DISTTRANSEND_ENABLED())
                MPI_QUERY_DISTTRANSEND(rank);
    return (unsigned short int *)fst;
}

//________________________________________________________________

void save_pgm(unsigned short int *m)
{
    FILE *pgmimg = fopen("new.pgm", "wb");
    // Writing Magic Number to the File
    fprintf(pgmimg, "P2\n");
    // Writing Width and Height
    fprintf(pgmimg, "%u %u\n", width - 1, height);
    // Writing the maximum gray value

    unsigned short int max_grey = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            if (m[i * width + j] >= max_grey)
                max_grey = m[i * width + j];
        }
    }
    fprintf(pgmimg, "%u\n", max_grey);
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            fprintf(pgmimg, "%u ", m[i * width + j]);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
}

unsigned char magic_num[2];
unsigned short gray_value = 0;
int brancostotal = 0;
unsigned short int *load_img(const char *filename)
{
    FILE *pgmimg = fopen(filename, "r");
    fseek(pgmimg, -1, SEEK_CUR);
    fscanf(pgmimg, "%s", magic_num);
    while (getc(pgmimg) == '#')
    { /* skip comment lines */
        while (getc(pgmimg) != '\n')
            ; /* skip to end of comment line */
    }
    fscanf(pgmimg, "%u", &width);
    fscanf(pgmimg, "%u", &height);
    fscanf(pgmimg, "%u", &gray_value);
    //Primeira posição do array é o numero de brancos
    width++;

    unsigned short int *img = (unsigned short int *)malloc(sizeof(unsigned short int) * height * width);
    int x;

    for (int i = 0; i < height; i++)
    {
        int brancosInLine = 0;
        for (int j = 1; j < width; j++)
        {
            fscanf(pgmimg, "%u", &x);
            if (x)
            {
                brancosInLine++;
                x = MAX;
            }
            img[i * width + j] = x;
            img[i * width] = brancosInLine;
        }
        brancostotal += brancosInLine;
    }
    fclose(pgmimg);
    return img;
}
//________________________________________________________________

void builddispMatrix(unsigned height, unsigned width, unsigned short int img[height][width], int displacement[numProc], int tamanhos[numProc])
{
    int tbrancos = height + brancostotal;
    int fraction = tbrancos / numProc;
    unsigned numLines = 0;
    unsigned currThread = 0;
    unsigned brancosThread = 0;
    int procRemaining = numProc;

    int disp = 0;
    for (int i = 0; i < height - 2; i++)
    {
        int numBrancos = img[i][0] + 1;
        int nextaux = img[i + 1][0] + 1;
        numLines++;
        brancosThread += numBrancos;
        if (fraction >= brancosThread && fraction < (brancosThread + nextaux))
        {
            tamanhos[currThread] = numLines * width;
            displacement[currThread] = disp * width;
            procRemaining--;
            tbrancos -= brancosThread;
            fraction = tbrancos / procRemaining;
            currThread++;
            disp += numLines;
            brancosThread = 0;
            numLines = 0;
        }
        if (procRemaining == 1)
        {
            numLines = height - i - 1;
            tamanhos[currThread] = numLines * width;
            displacement[currThread] = disp * width;
            i = height;
        }
    }
}

int main(int argc, char const *argv[])
{
    MPI_Init(NULL, NULL);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numProc);

    unsigned short int *img;
    double time;
    //Recebe trabalho das várias threads

    int size[2];
    if (rank == 0)
    {
        //Prepare stuff as rank0
        //Read Image
        img = load_img(argv[1]);
        size[0] = height;
        size[1] = width;
    }
#ifdef TRACK
    strcpy(filename, argv[2]);
#endif

    //Broadcast dos tamanhos
    MPI_Bcast(size, 2, MPI_INT, 0, MPI_COMM_WORLD);
    height = size[0];
    width = size[1];
    //numero de linhas atribuidas ao processo

    int above = 0;
    int bellow = 0;
    if (rank > 0)
        above++;
    if (rank < numProc - 1)
        bellow++;
    //Array dos displacements
    int displacement[numProc];
    //Array dos tamanhos atríbuidos a cada thread
    int tamanhos[numProc];
#ifdef STATIC
    int aux = 0;
    for (int i = 0; i < numProc; i++)
    {
        int l = height / numProc;
        if (height % numProc > i)
        {
            l = l + 1;
        }
        displacement[i] = aux;
        tamanhos[i] = l * width;
        aux += l * width;
    }
#else
    if (rank == 0)
    {
        builddispMatrix(height, width, (short unsigned int(*)[width])img, displacement, tamanhos);
    }
    MPI_Bcast(displacement, numProc, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(tamanhos, numProc, MPI_INT, 0, MPI_COMM_WORLD);
#endif
    int lines = tamanhos[rank] / width;
    //Array Recetor
    unsigned short int *rec = malloc(width * (lines + above + bellow) * sizeof(unsigned short int));
    unsigned short int *sec = malloc(width * (lines + above + bellow) * sizeof(unsigned short int));
#ifdef TESTE
    for (int tes = 0; tes < 10; tes++)
    {
        /* code */
#endif
        if (rank == 0)
            time = MPI_Wtime();

#ifdef TRACK
        time = MPI_Wtime();
        char PATHFILE[256];
        sprintf(PATHFILE, "%s/Rank_%d.txt", filename, rank);
        f = fopen(PATHFILE, "w");
#endif
        //Separa trabalho por threads
        MPI_Scatterv(img, tamanhos, displacement, MPI_UNSIGNED_SHORT, rec + above * width, lines * width, MPI_UNSIGNED_SHORT, 0, MPI_COMM_WORLD);

        //WORK-------------------------------------
        unsigned short int *res = distTransMPI(lines + above + bellow, above, bellow, width, (short unsigned int(*)[width])rec, (short unsigned int(*)[width])sec);
//----------------------------------------
#ifdef TRACK
        fprintf(f, "Time = %f\n", MPI_Wtime() - time);
        fclose(f);
#endif
        unsigned short int *final = (unsigned short int *)malloc(sizeof(unsigned short int) * height * width);
        MPI_Gatherv(res + width * above, lines * width, MPI_UNSIGNED_SHORT, final, tamanhos, displacement, MPI_UNSIGNED_SHORT, 0, MPI_COMM_WORLD);
#ifdef TESTE
        if (rank == 0)
            printf("Time = %f\n", MPI_Wtime() - time);
        free(final);
    }
#else
    if (rank == 0)
    {
        //printf("Time = %f\n", MPI_Wtime() - time);
        save_pgm(final);
    }
#endif
    MPI_Finalize();
    return 0;
}
