provider mpi
{
    /*(Rank)*/
    probe query__sendmessage(int);
    probe query__recievemessage(int);
    /*(Rank,brancos)*/
    probe query__startTransform(int,int);
    /*(Rank,brancos)*/
    probe query__endTransform(int,int);
    /*Process Line
    (Rank,brancos left)*/
    probe query__processLine(int,int);
    /*disTransStart
    (Rank,numLines)*/
    probe query__distTransStart(int,int);
    /*(Rank)*/
    probe query__distTransEnd(int);
};



mpi*:::query-distTransStart
{
    rank[arg0] = arg0;
    numlines[arg0] = arg1;
    numIts[arg0] = 0;
    i=0;
    disTime[arg0]= timestamp;
    totalTransformTime[arg0] = 0;
    sendTop[arg0] = 0;
    sendBot[arg0] = 0;
}

mpi*:::query-distTransEnd
{
    disTime[arg0]= timestamp-disTime[arg0];

    printf("Rank %d Number of Iterations %d NumLines %d\t",rank[arg0],numIts[arg0],numlines[arg0]);
    printf("Total Time %d, Tempo Computacional %d, Tempo Comunicacao %d\n", disTime[arg0]/1000000 ,totalTransformTime[arg0]/1000000, (disTime[arg0]-totalTransformTime[arg0])/1000000);
}



mpi*:::query-startTransform
{
    numIts[arg0]++;
    linesProcessed[arg0]=0;
    transformTime[arg0] = timestamp;
}

mpi*:::query-processLine
{
    linesProcessed[arg0]++;
}



mpi*:::query-endTransform
{
    transformTime[arg0] = timestamp-transformTime[arg0] ;
    totalTransformTime[arg0] = totalTransformTime[arg0] + transformTime[arg0];
    printf("Rank %d Iter %d Lines Processed %d, Lines Skipped %d,transformTime %d\n ",arg0,numIts[arg0],linesProcessed[arg0],numlines[arg0]-linesProcessed[arg0],transformTime[arg0]);
}

END
{

    printf("THE END");
}
