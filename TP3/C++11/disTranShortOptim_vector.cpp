#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <math.h>
#include <iostream>
#include <omp.h>
#include <vector>
#include <cstring>

#define MAX 0xFFFF

int numProc = 1;

int height;
int width;
std::vector<int> c;
std::vector<int> r;
std::vector<int> displacement;
std::vector<int> tamanhos;

unsigned transform(long height, int width, std::vector<int> fst, std::vector<int> snd, int above, int bellow)
{
    unsigned linebrancos = 0;
    unsigned brancos = 0;

    //printf("above = %d bellow = %d\n", above, bellow);
    //Mudados os limites nesta linha, para não processar linhas partilhadas
    for (int i = above; i < bellow; i++)
    {
        //printf("-------------I %d \n", i);
        linebrancos = 0;
        if (fst[i * width])
        {
            //alterada esta linha pq primeiro elemento do array contem o numero de brancos na linha
            for (int j = 1; j < width; j++)
            {
                //printf("-------------J %d \n", j);
                if (fst[i * width + j] == MAX)
                {
                    unsigned short min = MAX;
                    for (int y = -1; y < 2; y++)
                    {
                        for (int x = -1; x < 2; x++)
                        {
                            //Alterados os limites no processamento
                            if (i + y >= 0 && i + y < height && j + x >= 1 && j + x < width && !(y == 0 && x == 0))
                            {
                                unsigned short aux = fst[(i + y) * width + (j + x)];
                                if (aux < min)
                                    min = aux;
                            }
                        }
                    }
                    if (min >= MAX - 1)
                    {
                        snd[i * width + j] = MAX;
                        linebrancos++;
                    }
                    else
                    {
                        snd[i * width + j] = min + 1;
                    }
                }
                else
                    snd[i * width + j] = fst[i * width + j];
            }
            // Total de brancos guardada na primeira coluna da matriz
            snd[i * width] = linebrancos;
        }
        //Se a linha fst nao tiver brancos e a snd for diferente copia o array
        if (!fst[i * width] && snd[i * width])
        {
            memcpy(&(snd[i * width]), &(fst[i * width]), width * sizeof(unsigned short int));
        }
        brancos += linebrancos;
    }
    return brancos;
}

int brancosGlobal = 1;
void distTrans()
{
    int th = 0;
    unsigned brancos = 1;
    int lines = tamanhos[th];
    int start = displacement[th];
    int end = start + lines;

    std::vector<int> aux;
    std::vector<int> cc = c;
    std::vector<int> rr = r;

    printf("Thread %d displacement = %d tamanho = %d processadas = %d\n", th, start, lines, end);

    int brancosOld = 0;
    while (brancosGlobal > brancosOld)
    {
        brancosOld = brancosGlobal;

        brancos = transform(height, width, cc, rr, start, end);

        brancosGlobal += brancos;

        aux = cc;
        cc = rr;
        rr = aux;
    }
    if (th == 0)
    {
        c = cc;
        r = rr;
    }
}

//________________________________________________________________

void save_pgm(std::vector<int> m)
{
    FILE *pgmimg = fopen("new.pgm", "wb");
    // Writing Magic Number to the File
    fprintf(pgmimg, "P2\n");
    // Writing Width and Height
    fprintf(pgmimg, "%u %u\n", width - 1, height);
    // Writing the maximum gray value

    unsigned short int max_grey = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            if (m[i * width + j] >= max_grey)
                max_grey = m[i * width + j];
        }
    }
    fprintf(pgmimg, "%u\n", max_grey);
    for (int i = 0; i < height; i++)
    {
        for (int j = 1; j < width; j++)
        {
            fprintf(pgmimg, "%u ", m[i * width + j]);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
}

unsigned char magic_num[2];
unsigned gray_value = 0;
int brancostotal = 0;

std::vector<int> load_img(const char *filename)
{
    FILE *pgmimg = fopen(filename, "r");
    fseek(pgmimg, -1, SEEK_CUR);
    fscanf(pgmimg, "%s", magic_num);
    while (getc(pgmimg) == '#')
    { /* skip comment lines */
        while (getc(pgmimg) != '\n')
            ; /* skip to end of comment line */
    }
    fscanf(pgmimg, "%u", &width);
    fscanf(pgmimg, "%u", &height);
    fscanf(pgmimg, "%u", &gray_value);
    //Primeira posição do array é o numero de brancos
    width++;

    std::vector<int> img(height * width);
    int x;

    for (int i = 0; i < height; i++)
    {
        int brancosInLine = 0;
        for (int j = 1; j < width; j++)
        {
            fscanf(pgmimg, "%u", &x);
            if (x)
            {
                brancosInLine++;
                x = MAX;
            }
            img[i * width + j] = x;
            img[i * width] = brancosInLine;
        }
        brancostotal += brancosInLine;
    }
    fclose(pgmimg);
    return img;
}
//________________________________________________________________

void builddispMatrix(int height, int width, std::vector<int> img, std::vector<int> displacement, std::vector<int> tamanhos)
{
    int tbrancos = height + brancostotal;
    int fraction = tbrancos / numProc;
    unsigned numLines = 0;
    unsigned currThread = 0;
    int brancosThread = 0;
    int procRemaining = numProc;

    int disp = 0;
    for (int i = 0; i < height - 2; i++)
    {
        int numBrancos = img[i * width] + 1;
        int nextaux = img[(i + 1) * width] + 1;
        numLines++;
        brancosThread += numBrancos;
        if (fraction >= brancosThread && fraction < (brancosThread + nextaux))
        {
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            procRemaining--;
            tbrancos -= brancosThread;
            fraction = tbrancos / procRemaining;
            currThread++;
            disp += numLines;
            brancosThread = 0;
            numLines = 0;
        }
        if (procRemaining == 1)
        {
            numLines = height - i - 1;
            tamanhos[currThread] = numLines;
            displacement[currThread] = disp;
            i = height;
        }
    }
}

int main(int argc, char const *argv[])
{
    if (argc >= 2 && argc <= 3)
    {
        if (argc == 3)
            numProc = atoi(argv[2]);
        printf("Threads = %d\n", numProc);

        c = load_img(argv[1]);
        r.resize(height * width);

        printf("Img = %s\nHeight = %d Width = %d\n", argv[1], height, width - 1);

        //Array dos displacements
        displacement.resize(numProc);
        //Array dos tamanhos atríbuidos a cada thread
        tamanhos.resize(numProc);

        builddispMatrix(height, width, c, displacement, tamanhos);

        double time = omp_get_wtime();
        distTrans();
        printf("Time = %f\n", omp_get_wtime() - time);

        save_pgm(c);
    }
    return 0;
}
