#ifndef BARRIER_H_
#define BARRIER_H_

#include <errno.h>
#include <condition_variable>

typedef struct
{
    std::mutex mutex;
    std::condition_variable cond;
    int count;
    int tripCount;
} barrier_t;

int barrier_init(barrier_t *barrier, unsigned int count)
{
    if (count == 0)
    {
        errno = EINVAL;
        return -1;
    }
    barrier->tripCount = count;
    barrier->count = 0;
    return 0;
}

int barrier_wait(barrier_t *barrier)
{
    std::unique_lock<std::mutex> lock(barrier->mutex);
    ++(barrier->count);
    if (barrier->count >= barrier->tripCount)
    {
        barrier->count = 0;
        barrier->cond.notify_all();
        lock.unlock();
        return 1;
    }
    else
    {
        barrier->cond.wait(lock);
        lock.unlock();
        return 0;
    }
}

#endif // BARRIER_H_
