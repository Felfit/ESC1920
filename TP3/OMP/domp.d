
provider omp
{
    probe query__startloading();
    probe query__endloading();
    probe query__startsaving();
    probe query__endsaving();
    probe query__startcomput();
    probe query__endcomput();
    probe query__imagesize(int, int, int);
    probe query__numiter(int);
    probe query__initthread();
    probe query__endthread();
};

BEGIN
{
    iter = 0;
    start = timestamp;
}

omp*:::query-startloading
{
    start_load = timestamp;
}

omp*:::query-endloading
{
    end_load = timestamp;
}

omp*:::query-startsaving
{
    start_save = timestamp;
}

omp*:::query-endsaving
{
    end_saving = timestamp;
}

omp*:::query-startcomput
{
    start_comput = timestamp;
}

omp*:::query-endcomput
{
    end_comput = timestamp;
}

omp*:::query-imagesize
{
    height = arg0;
    width = arg1;
    gray_value = arg2;
}

omp*:::query-numiter
{
    iter = iter + 1;
    @br = quantize(arg0);
}

omp*:::query-initthread
{
    auxth[tid] = vtimestamp;
}

omp*:::query-endthread
{
    @th[tid] = sum(vtimestamp - auxth[tid]);
    @qth = lquantize(tid, 1, 8);
}

END
{
    printf("\t-------------------------------------\n");
    printf("\t---        Tracing Reports        ---\n");
    printf("\t-------------------------------------\n");
    printf("\n\tPID: %d UID: %d GID: %d\n",
        curpsinfo->pr_pid,
        curpsinfo->pr_uid,
        curpsinfo->pr_gid
    );
    printf("\n\tImage values:\n\tHeight = %d Width = %d Gray = %d\n", height, width, gray_value);
    printf("\n\tTotal Time: %d\n", timestamp - start);
    printf("\tTime loading: %d\n", end_load - start_load);
    printf("\tTime saving: %d\n", end_saving - start_save);
    printf("\tComputational Time: %d\n\n", end_comput - start_comput);
    printa("\tThread %d workload: %@d\n", @th);
    clear(@th);
    printf("\n\tThread usage: ");
    printa(@qth);
    clear(@qth);
    printf("\n\tWhites by iteration: ");
    printa(@br);
    clear(@br);
    printf("\tTotal number of iterations: %d\n", iter);
}

