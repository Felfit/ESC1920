#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <stdlib.h>
#include "domp.h"

FILE *pgmimg;
unsigned char magic_num[2];
unsigned int width = 0, height = 0;
unsigned short gray_value = 0;
unsigned short **a;
unsigned short **r;
#define MAX 0xFFFF

int transform(int height, int width, unsigned short **c, unsigned short **r, char hasBrancos[height])
{
    int brancos = 0;
    int initbrancos;
#pragma omp parallel for reduction(+ \
                                   : brancos) schedule(dynamic) private(initbrancos)
    for (int i = 0; i < height; i++)
    {
        if (OMP_QUERY_INITTHREAD_ENABLED())
            OMP_QUERY_INITTHREAD();
        initbrancos = 0;
        if (hasBrancos[i])
        {
            for (int j = 0; j < width; j++)
            {
                if (c[i][j] == MAX)
                {
                    unsigned short min = MAX;
                    for (int y = -1; y < 2; y++)
                    {
                        for (int x = -1; x < 2; x++)
                        {
                            if (i + y >= 0 && i + y < height && j + x >= 0 && j + x < width && !(y == 0 && x == 0))
                            {
                                unsigned short aux = c[i + y][j + x];
                                if (aux < min)
                                    min = aux;
                            }
                        }
                    }
                    if (min >= MAX - 1)
                    {
                        r[i][j] = MAX;
                        initbrancos++;
                    }
                    else
                        r[i][j] = min + 1;
                }
                else
                    r[i][j] = c[i][j];
            }
            if (initbrancos == 0)
            {
                hasBrancos[i]--;
            }
        }
        brancos += initbrancos;
        if (OMP_QUERY_ENDTHREAD_ENABLED())
            OMP_QUERY_ENDTHREAD();
    }
    return brancos;
}

int distTrans(int height, int width, unsigned short **c, unsigned short **r)
{
    int size = height * width;
    int brancos = 1;
    char hasBrancos[height];
#pragma omp parallel for schedule(static)
    for (int i = 0; i < height; i++)
    {
        hasBrancos[i] = 2;
    }
    while (1)
    {
        if (brancos)
        {
            brancos = transform(height, width, c, r, hasBrancos);
            if (OMP_QUERY_NUMITER_ENABLED())
                OMP_QUERY_NUMITER(brancos);
        }
        else
            return 1;
        if (brancos)
        {
            brancos = transform(height, width, r, c, hasBrancos);
            if (OMP_QUERY_NUMITER_ENABLED())
                OMP_QUERY_NUMITER(brancos);
        }
        else
            return 2;
    }
    return -1;
}

//________________________________________________________________

void load_img(char *filename)
{
    FILE *pgmimg = fopen(filename, "r");
    fseek(pgmimg, -1, SEEK_CUR);
    fscanf(pgmimg, "%s", magic_num);
    while (getc(pgmimg) == '#')
    { /* skip comment lines */
        while (getc(pgmimg) != '\n')
            ; /* skip to end of comment line */
    }
    fscanf(pgmimg, "%u", &width);
    fscanf(pgmimg, "%u", &height);
    fscanf(pgmimg, "%u", &gray_value);
    if (OMP_QUERY_IMAGESIZE_ENABLED())
        OMP_QUERY_IMAGESIZE(width, height, gray_value);
    a = (unsigned short **)malloc(height * sizeof(unsigned short *));
    r = (unsigned short **)malloc(height * sizeof(unsigned short *));
    unsigned short x;
    for (int i = 0; i < height; i++)
    {
        a[i] = (unsigned short *)malloc(width * sizeof(unsigned short));
        r[i] = (unsigned short *)malloc(width * sizeof(unsigned short));
        for (int j = 0; j < width; j++)
        {
            fscanf(pgmimg, "%u", &x);
            if (x)
                x = MAX;
            a[i][j] = x;
        }
    }
    fclose(pgmimg);
}

void save_pgm(unsigned short **m)
{
    FILE *pgmimg = fopen("../Images/new.pgm", "wb");
    // Writing Magic Number to the File
    fprintf(pgmimg, "P2\n");
    // Writing Width and Height
    fprintf(pgmimg, "%u %u\n", width, height);
    // Writing the maximum gray value
    unsigned short max_grey = 0;
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            if (m[i][j] >= max_grey)
                max_grey = m[i][j];
        }
    }
    fprintf(pgmimg, "%u\n", max_grey);
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            fprintf(pgmimg, "%u ", m[i][j]);
        }
        fprintf(pgmimg, "\n");
    }
    fclose(pgmimg);
}

//________________________________________________________________

int main(int argc, char **argv)
{
    if (argc == 2)
    {
        if (OMP_QUERY_STARTLOADING_ENABLED())
            OMP_QUERY_STARTLOADING();
        load_img(argv[1]);
        if (OMP_QUERY_ENDLOADING_ENABLED())
            OMP_QUERY_ENDLOADING();
        //printf("Img = %s\nHeight = %d Width = %d\n", argv[1], height, width);
        //double time = omp_get_wtime();
        if (OMP_QUERY_STARTCOMPUT_ENABLED())
            OMP_QUERY_STARTCOMPUT();
        int x = distTrans(height, width, a, r);
        if (OMP_QUERY_ENDCOMPUT_ENABLED())
            OMP_QUERY_ENDCOMPUT();
        //printf("Time = %f\n", omp_get_wtime() - time);
        if (x == 1)
        {
            if (OMP_QUERY_STARTSAVING_ENABLED())
                OMP_QUERY_STARTSAVING();
            save_pgm(a);
            if (OMP_QUERY_ENDSAVING_ENABLED())
                OMP_QUERY_ENDSAVING();
        }
        else
        {
            if (OMP_QUERY_STARTSAVING_ENABLED())
                OMP_QUERY_STARTSAVING();
            save_pgm(r);
            if (OMP_QUERY_ENDSAVING_ENABLED())
                OMP_QUERY_ENDSAVING();
        }
        free(a);
        free(r);
    }
    else
    {
        printf("No input!!!\n");
    }
    return 0;
}
