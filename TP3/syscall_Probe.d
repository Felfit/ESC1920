this string flag;

syscall::openat:entry
{
	path = copyinstr(arg1);
    flags = arg2;
    pathstart =  path;
    pathstart[8] = '\0';
    pathstart2 =  path;
    pathstart2[9] = '\0';
}

syscall::openat:return
/ ( pathstart == "./Images"|| pathstart2 == "../Images") /
{
    error = errno ? "Yes" : "No";
    this->flag = strjoin(
        flags & O_WRONLY ? "O_WRONLY" : flags & O_RDWR ? "O_RDWR": "O_RDONLY",
            strjoin(
                flags & O_APPEND  ? "|O_APPEND" : "",
                flags & O_CREAT   ? "|O_CREAT" : "")
            );
    printf("\n\t===== Openat =====");
    printf("\n\tExecutavel: %s\n\tPID: %d UID: %d GID: %d \n\tPath: %s\n\tFlags: %s\n\tError: %s",
        execname,
        curpsinfo->pr_pid,
        curpsinfo->pr_uid,
        curpsinfo->pr_gid,
        path,
        this->flag,
        error
    );
}

