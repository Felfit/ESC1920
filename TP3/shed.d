
sched:::on-cpu
/ execname == "disTranShortOpti" /
{
	self->ts = timestamp;
}

sched:::off-cpu
/ execname == "disTranShortOpti" && self->ts/
{
	@quant[cpu] = quantize(timestamp - self->ts);
    @sum[cpu] = sum(timestamp - self->ts);
	self->ts = 0;
}

END
{
    printa(@quant);
	printf("CPU microseconds\n--- ------------\n");
	printa("%3d %@d\n", @sum);
    clear(@quant);
    clear(@sum);
}
