#!/bin/sh

dtrace -s ../cpc_tot_ins.d -c "./omp.sh"
dtrace -s ../cpc_tot_cyc.d -c "./omp.sh"
dtrace -s ../cpc_l1_dcm.d -c "./omp.sh"
dtrace -s ../cpc_l2_dcm.d -c "./omp.sh"
