#!/bin/sh

echo "SEQ IMG"
dtrace -s ../cpc_tot_ins.d -c "../SEQ/disTranShortOptim ../Images/img.pgm"
dtrace -s ../cpc_tot_cyc.d -c "../SEQ/disTranShortOptim ../Images/img.pgm"
dtrace -s ../cpc_l1_dcm.d -c "../SEQ/disTranShortOptim ../Images/img.pgm"
dtrace -s ../cpc_l2_dcm.d -c "../SEQ/disTranShortOptim ../Images/img.pgm"

for i in 2 4 8
do
    echo "=================== THREADS = $i =========================="
    echo "OMP IMG\n"
    export OMP_NUM_THREADS=$i
    dtrace -s ../cpc_tot_ins.d -c "../OMP/disTranShortOptim ../Images/img.pgm"
    dtrace -s ../cpc_tot_cyc.d -c "../OMP/disTranShortOptim ../Images/img.pgm"
    dtrace -s ../cpc_l1_dcm.d -c "../OMP/disTranShortOptim ../Images/img.pgm"
    dtrace -s ../cpc_l2_dcm.d -c "../OMP/disTranShortOptim ../Images/img.pgm"
    echo "Pthreads IMG\n"
    dtrace -s ../cpc_tot_ins.d -c "../Pthreads/disTranShortOptim ../Images/img.pgm $i"
    dtrace -s ../cpc_tot_cyc.d -c "../Pthreads/disTranShortOptim ../Images/img.pgm $i"
    dtrace -s ../cpc_l1_dcm.d -c "../Pthreads/disTranShortOptim ../Images/img.pgm $i"
    dtrace -s ../cpc_l2_dcm.d -c "../Pthreads/disTranShortOptim ../Images/img.pgm $i"
    echo "C++11 IMG\n"
    dtrace -s ../cpc_tot_ins.d -c "../C++11/disTranShortOptim ../Images/img.pgm $i"
    dtrace -s ../cpc_tot_cyc.d -c "../C++11/disTranShortOptim ../Images/img.pgm $i"
    dtrace -s ../cpc_l1_dcm.d -c "../C++11/disTranShortOptim ../Images/img.pgm $i"
    dtrace -s ../cpc_l2_dcm.d -c "../C++11/disTranShortOptim ../Images/img.pgm $i"
done
