
provider seq
{
    probe query__startloading();
    probe query__endloading();
    probe query__startsaving();
    probe query__endsaving();
    probe query__startcomput();
    probe query__endcomput();
    probe query__imagesize(int, int, int);
    probe query__numiter(int);
};

BEGIN
{
    iter = 0;
    start = timestamp;
}

seq*:::query-startloading
{
    start_load = timestamp;
}

seq*:::query-endloading
{
    end_load = timestamp;
}

seq*:::query-startsaving
{
    start_save = timestamp;
}

seq*:::query-endsaving
{
    end_saving = timestamp;
}

seq*:::query-startcomput
{
    start_comput = timestamp;
}

seq*:::query-endcomput
{
    end_comput = timestamp;
}

seq*:::query-imagesize
{
    height = arg0;
    width = arg1;
    gray_value = arg2;
}

seq*:::query-numiter
{
    iter = iter + 1;
    @br = quantize(arg0);
}

END
{
    printf("\t-------------------------------------\n");
    printf("\t---        Tracing Reports        ---\n");
    printf("\t-------------------------------------\n");
    printf("\n\tPID: %d UID: %d GID: %d\n",
        curpsinfo->pr_pid,
        curpsinfo->pr_uid,
        curpsinfo->pr_gid
    );
    printf("\n\tImage values:\n\tHeight = %d Width = %d Gray = %d\n", height, width, gray_value);
    printf("\n\tTotal Time: %d\n", timestamp - start);
    printf("\tTime loading: %d\n", end_load - start_load);
    printf("\tTime saving: %d\n", end_saving - start_save);
    printf("\tComputational Time: %d\n", end_comput - start_comput);
    printf("\n\tWhites by iteration: ");
    printa(@br);
    trunc(@br);
    printf("\tTotal number of iterations: %d\n", iter);
}

