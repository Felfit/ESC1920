#!/bin/sh
#PBS -l nodes=1:r652:ppn=40
#PBS -l walltime=02:00:00

cd ESC/sort

echo $HOSTNAME

module load gcc/5.3.0/

g++ -g -O3 -std=c99 -Wall -o sort main.c sort.c sort.h -lm 

mkdir results1a
for i in 1 2 3 4
do 
    perf stat -e instructions,cache-misses ./sort $i 1 100000000  >results1a/sort$i.txt 2>results1a/sorterr$i.txt
done

mkdir results1b
for i in 1 2 3 4
do
    perf record -F 99 ./sort $i 1 100000000 >results1b/sortrecord$i.txt 2>results1b/sortrecorderr$i.txt 
    perf report -n --stdio >>results1b/sortrecord$i.txt 2>>results1b/sortrecorderr$i.txt 
done

#mkdir results1c
for i in 1 2 3 4
do
    echo sort$i.txt
    perf record -F 99 -ag ./sort $i 1 100000000 
    perf script >results1c/stackcollageinput$i.txt
done
#
#mkdir results1d
#
for i in 1 2 3 4
do
    perf record -g ./sort $i 1 100000000  
    perf annotate --stdio >results1d/sort$i.txt
done