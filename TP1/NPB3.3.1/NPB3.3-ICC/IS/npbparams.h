#define CLASS 'C'
/*
   This file is generated automatically by the setparams utility.
   It sets the number of processors and the class of the NPB
   in this directory. Do not modify it by hand.   */
   
#define COMPILETIME "16 Apr 2020"
#define NPBVERSION "3.3.1"
#define CC "icc"
#define CFLAGS "-O2  -fopenmp"
#define CLINK "$(CC)"
#define CLINKFLAGS "-O2  -fopenmp"
#define C_LIB "(none)"
#define C_INC "(none)"
