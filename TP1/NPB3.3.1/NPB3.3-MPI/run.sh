#!/bin/sh

for j in A B C #Class
    do
        for k in bt ft is #kernel
        do
            for l in 25 32 #threads
            do
                #make $k CLASS=$j NPROCS=$l
                DIR="results/"$k"/Comp_"$i"/O3"
                mkdir -p $DIR
                #export OMP_NUM_THREADS=$l
                mpirun -np $l ./bin/$k.$j.$l > $DIR/$k.$j.$l.baseline
            done
        done
    done
