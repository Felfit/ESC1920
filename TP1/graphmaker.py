import matplotlib

import os
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


import re
def processFile(path):
    ###
    f = open(path,"r")
    
    a = re.findall("\s(\S.*?)\s*?=\s*(\S.*)",f.read())
    out = {}
    for key,val in a:
        out[key]=val 

    f.close()
    ###
    return out


def barGrafBuilder(arrs,labels,title=""):
    A = arrs[:,0]
    B = arrs[:,1]
    C = arrs[:,2]

    ind = np.arange(len(arrs))  # the x locations for the groups
    width = 0.25  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind - width, A, width, label='A')
    rects3 = ax.bar(ind  , B, width, label='B')
    rects2 = ax.bar(ind + width, C, width, label='C')

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Mop/s')
    ax.set_title(title)
    ax.set_xticks(ind)
    ax.set_xticklabels(labels)
    ax.legend()

    plt.savefig(title+'.png')

folders = [
    "NPB3.3.1/NPB3.3-ICC",
    "NPB3.3.1/NPB3.3-MPI",
    "NPB3.3.1/NPB3.3-OMP",
    "NPB3.3.1/NPB3.3-SER"
    ]
#normalKern = readFolders(folders)

foldersMZ = [
    "NPB3.3.1-MZ/NPB3.3-MZ-MPI",
    "NPB3.3.1-MZ/NPB3.3-MZ-OMP",
    "NPB3.3.1-MZ/NPB3.3-MZ-SER"
    ]
#hybridKern = readFolders(foldersMZ)

#test = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_2.0.0/O3/bt-mz.C.32.baseline")                  
#for t in test:
#    print(t)

##GRAFICOS



import matplotlib
import matplotlib.pyplot as plt
import numpy as np


arr= np.array([
        [20, 35, 30],
        [25, 32, 34],
        [25, 32, 34],
        [ 2,  2,  2]
            ])
labels = ("ICC","ICC","GCC","GCC")
# -OMP VS MPI VS Serial
def isOMPCOMP():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.A.x.baseline").get("Mop/s total",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.B.x.baseline").get("Mop/s total",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.C.x.baseline").get("Mop/s total",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/is/Comp_1.8.2/O2/is.A.32.baseline").get("Mop/s total",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/is/Comp_1.8.2/O2/is.B.32.baseline").get("Mop/s total",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/is/Comp_1.8.2/O2/is.C.32.baseline").get("Mop/s total",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/is/Comp_5.3.0/O2/is.A.x.baseline").get("Mop/s total",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/is/Comp_5.3.0/O2/is.B.x.baseline").get("Mop/s total",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/is/Comp_5.3.0/O2/is.C.x.baseline").get("Mop/s total",0)
    print(AUX)

    labels = ("OMP","MPI","SER")

    barGrafBuilder(AUX,labels,title="OMP vs MPI vs SERIAL - IS Kernel")


def ftOMPCOMP():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.A.x.baseline").get("Mop/s total",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.B.x.baseline").get("Mop/s total",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.C.x.baseline").get("Mop/s total",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/ft/Comp_1.8.2/O2/ft.A.32.baseline").get("Mop/s total",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/ft/Comp_1.8.2/O2/ft.B.32.baseline").get("Mop/s total",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/ft/Comp_1.8.2/O2/ft.C.32.baseline").get("Mop/s total",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/ft/Comp_5.3.0/O2/ft.A.x.baseline").get("Mop/s total",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/ft/Comp_5.3.0/O2/ft.B.x.baseline").get("Mop/s total",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/ft/Comp_5.3.0/O2/ft.C.x.baseline").get("Mop/s total",0)
    print(AUX)

    labels = ("OMP","MPI","SER")

    barGrafBuilder(AUX,labels,title="OMP vs MPI vs SERIAL - FT Kernel")
def btOMPComp():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.A.x.baseline").get("Mop/s total",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.B.x.baseline").get("Mop/s total",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.C.x.baseline").get("Mop/s total",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/bt/Comp_1.8.2/O2/bt.A.25.baseline").get("Mop/s total",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/bt/Comp_1.8.2/O2/bt.B.25.baseline").get("Mop/s total",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/bt/Comp_1.8.2/O2/bt.C.25.baseline").get("Mop/s total",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/bt/Comp_5.3.0/O2/bt.A.x.baseline").get("Mop/s total",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/bt/Comp_5.3.0/O2/bt.B.x.baseline").get("Mop/s total",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/bt/Comp_5.3.0/O2/bt.C.x.baseline").get("Mop/s total",0)
    print(AUX)

    labels = ("OMP","MPI","SER")

    barGrafBuilder(AUX,labels,title="OMP vs MPI vs SERIAL - BT Kernel")


def hybrid():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-SER/results/bt-mz/Comp_7.2.0/O3/bt-mz.A.x.baseline").get("Mop/s total",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-SER/results/bt-mz/Comp_7.2.0/O3/bt-mz.B.x.baseline").get("Mop/s total",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-SER/results/bt-mz/Comp_7.2.0/O3/bt-mz.C.x.baseline").get("Mop/s total",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-OMP/results/bt-mz/Comp_7.2.0/O3/bt-mz.A.x.baseline").get("Mop/s total",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-OMP/results/bt-mz/Comp_7.2.0/O3/bt-mz.B.x.baseline").get("Mop/s total",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-OMP/results/bt-mz/Comp_7.2.0/O3/bt-mz.C.x.baseline").get("Mop/s total",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_1.8.2/O3/bt-mz.A.16.baseline").get("Mop/s total",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_1.8.2/O3/bt-mz.B.32.baseline").get("Mop/s total",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_1.8.2/O3/bt-mz.C.32.baseline").get("Mop/s total",0)
    print(AUX)

    labels =("SER","OMP","Hybrid")

    barGrafBuilder(AUX,labels,title="MOPs Serial vs Hybrid vs MPI")

def hybridTimes():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-SER/results/bt-mz/Comp_7.2.0/O3/bt-mz.A.x.baseline").get("Time in seconds",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-SER/results/bt-mz/Comp_7.2.0/O3/bt-mz.B.x.baseline").get("Time in seconds",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-SER/results/bt-mz/Comp_7.2.0/O3/bt-mz.C.x.baseline").get("Time in seconds",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-OMP/results/bt-mz/Comp_7.2.0/O3/bt-mz.A.x.baseline").get("Time in seconds",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-OMP/results/bt-mz/Comp_7.2.0/O3/bt-mz.B.x.baseline").get("Time in seconds",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-OMP/results/bt-mz/Comp_7.2.0/O3/bt-mz.C.x.baseline").get("Time in seconds",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_1.8.2/O3/bt-mz.A.16.baseline").get("Time in seconds",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_1.8.2/O3/bt-mz.B.32.baseline").get("Time in seconds",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1-MZ/NPB3.3-MZ-MPI/results/bt-mz/Comp_1.8.2/O3/bt-mz.C.32.baseline").get("Time in seconds",0)
    print(AUX)

    labels = ("SER","OMP","Hybrid")

    barGrafBuilder(AUX,labels,title="Times Serial vs Hybrid vs MPI")
def isOMPCOMPTimes():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.A.x.baseline").get("Time in seconds",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.B.x.baseline").get("Time in seconds",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.C.x.baseline").get("Time in seconds",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/is/Comp_1.8.2/O2/is.A.32.baseline").get("Time in seconds",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/is/Comp_1.8.2/O2/is.B.32.baseline").get("Time in seconds",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/is/Comp_1.8.2/O2/is.C.32.baseline").get("Time in seconds",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/is/Comp_5.3.0/O2/is.A.x.baseline").get("Time in seconds",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/is/Comp_5.3.0/O2/is.B.x.baseline").get("Time in seconds",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/is/Comp_5.3.0/O2/is.C.x.baseline").get("Time in seconds",0)
    print(AUX)

    labels = ("OMP","MPI","SER")

    barGrafBuilder(AUX,labels)

def ftOMPCOMPTimes():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.A.x.baseline").get("Time in seconds",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.B.x.baseline").get("Time in seconds",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.C.x.baseline").get("Time in seconds",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/ft/Comp_1.8.2/O2/ft.A.32.baseline").get("Time in seconds",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/ft/Comp_1.8.2/O2/ft.B.32.baseline").get("Time in seconds",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/ft/Comp_1.8.2/O2/ft.C.32.baseline").get("Time in seconds",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/ft/Comp_5.3.0/O2/ft.A.x.baseline").get("Time in seconds",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/ft/Comp_5.3.0/O2/ft.B.x.baseline").get("Time in seconds",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/ft/Comp_5.3.0/O2/ft.C.x.baseline").get("Time in seconds",0)
    print(AUX)

    labels = ("OMP","MPI","SER")

    barGrafBuilder(AUX,labels)
def btOMPCompTimes():
    AUX = np.zeros([3,3])
    AUX[0][0] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.A.x.baseline").get("Time in seconds",0)  
    AUX[0][1] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.B.x.baseline").get("Time in seconds",0)                    
    AUX[0][2] = processFile("TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.C.x.baseline").get("Time in seconds",0)
    AUX[1][0] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/bt/Comp_1.8.2/O2/bt.A.25.baseline").get("Time in seconds",0)  
    AUX[1][1] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/bt/Comp_1.8.2/O2/bt.B.25.baseline").get("Time in seconds",0)                    
    AUX[1][2] = processFile("TP1/NPB3.3.1/NPB3.3-MPI/results/bt/Comp_1.8.2/O2/bt.C.25.baseline").get("Time in seconds",0)
    AUX[2][0] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/bt/Comp_5.3.0/O2/bt.A.x.baseline").get("Time in seconds",0)  
    AUX[2][1] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/bt/Comp_5.3.0/O2/bt.B.x.baseline").get("Time in seconds",0)                    
    AUX[2][2] = processFile("TP1/NPB3.3.1/NPB3.3-SER/results/bt/Comp_5.3.0/O2/bt.C.x.baseline").get("Time in seconds",0)
    print(AUX)

    labels = ("OMP","MPI","SER")

    barGrafBuilder(AUX,labels)
def gbt():
    AUX = np.zeros([3, 3])
    AUX[0][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[0][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[0][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O/bt.C.x.baseline").get("Mop/s total", 0)
    AUX[1][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O2/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[1][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O2/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[1][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O2/bt.C.x.baseline").get("Mop/s total", 0)
    AUX[2][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O3/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[2][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O3/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[2][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O3/bt.C.x.baseline").get("Mop/s total", 0)

    labels = ("-O", "-O2", "-O3")
    barGrafBuilder(AUX, labels, title="MOP\'s for different compiler options - BT Kernel")


def gft():
    AUX = np.zeros([3, 3])
    AUX[0][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[0][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[0][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O/ft.C.x.baseline").get("Mop/s total", 0)
    AUX[1][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O2/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[1][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O2/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[1][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O2/ft.C.x.baseline").get("Mop/s total", 0)
    AUX[2][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O3/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[2][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O3/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[2][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O3/ft.C.x.baseline").get("Mop/s total", 0)

    labels = ("-O", "-O2", "-O3")
    barGrafBuilder(AUX, labels, title="MOP\'s for different compiler options - FT Kernel")


def gis():
    AUX = np.zeros([3, 3])
    AUX[0][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O/is.A.x.baseline").get("Mop/s total", 0)
    AUX[0][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O/is.B.x.baseline").get("Mop/s total", 0)
    AUX[0][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O/is.C.x.baseline").get("Mop/s total", 0)
    AUX[1][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O2/is.A.x.baseline").get("Mop/s total", 0)
    AUX[1][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O2/is.B.x.baseline").get("Mop/s total", 0)
    AUX[1][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O2/is.C.x.baseline").get("Mop/s total", 0)
    AUX[2][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O3/is.A.x.baseline").get("Mop/s total", 0)
    AUX[2][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O3/is.B.x.baseline").get("Mop/s total", 0)
    AUX[2][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O3/is.C.x.baseline").get("Mop/s total", 0)

    labels = ("-O", "-O2", "-O3")
    barGrafBuilder(AUX, labels, title="MOP\'s for different compiler options - IS Kernel")


def cbt():
    AUX = np.zeros([4, 3])
    AUX[0][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[0][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[0][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_5.3.0/O2/bt.C.x.baseline").get("Mop/s total", 0)
    AUX[1][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O2/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[1][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O2/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[1][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/bt/Comp_7.2.0/O2/bt.C.x.baseline").get("Mop/s total", 0)
    AUX[2][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/bt/Comp_2019/O3/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[2][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/bt/Comp_2019/O3/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[2][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/bt/Comp_2019/O3/bt.C.x.baseline").get("Mop/s total", 0)
    AUX[3][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/bt/Comp_2020/O3/bt.A.x.baseline").get("Mop/s total", 0)
    AUX[3][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/bt/Comp_2020/O3/bt.B.x.baseline").get("Mop/s total", 0)
    AUX[3][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/bt/Comp_2020/O3/bt.C.x.baseline").get("Mop/s total", 0)

    labels = ("GCC 5.3.0", "GCC 7.2.0", "ICC 2019", "ICC 2020")
    barGrafBuilder(
        AUX, labels, title="Mop\'s for different compiler versions - BT kernel")


def cft():
    AUX = np.zeros([4, 3])
    AUX[0][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[0][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[0][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_5.3.0/O2/ft.C.x.baseline").get("Mop/s total", 0)
    AUX[1][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O2/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[1][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O2/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[1][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/ft/Comp_7.2.0/O2/ft.C.x.baseline").get("Mop/s total", 0)
    AUX[2][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/ft/Comp_2019/O3/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[2][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/ft/Comp_2019/O3/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[2][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/ft/Comp_2019/O3/ft.C.x.baseline").get("Mop/s total", 0)
    AUX[3][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/ft/Comp_2020/O3/ft.A.x.baseline").get("Mop/s total", 0)
    AUX[3][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/ft/Comp_2020/O3/ft.B.x.baseline").get("Mop/s total", 0)
    AUX[3][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/ft/Comp_2020/O3/ft.C.x.baseline").get("Mop/s total", 0)

    labels = ("GCC 5.3.0", "GCC 7.2.0", "ICC 2019", "ICC 2020")
    barGrafBuilder(
        AUX, labels, title="Mop\'s for different compiler versions - FT kernel")


def cis():
    AUX = np.zeros([4, 3])
    AUX[0][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.A.x.baseline").get("Mop/s total", 0)
    AUX[0][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.B.x.baseline").get("Mop/s total", 0)
    AUX[0][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_5.3.0/O2/is.C.x.baseline").get("Mop/s total", 0)
    AUX[1][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O2/is.A.x.baseline").get("Mop/s total", 0)
    AUX[1][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O2/is.B.x.baseline").get("Mop/s total", 0)
    AUX[1][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-OMP/results/is/Comp_7.2.0/O2/is.C.x.baseline").get("Mop/s total", 0)
    AUX[2][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/is/Comp_2019/O3/is.A.x.baseline").get("Mop/s total", 0)
    AUX[2][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/is/Comp_2019/O3/is.B.x.baseline").get("Mop/s total", 0)
    AUX[2][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/is/Comp_2019/O3/is.C.x.baseline").get("Mop/s total", 0)
    AUX[3][0] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/is/Comp_2020/O3/is.A.x.baseline").get("Mop/s total", 0)
    AUX[3][1] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/is/Comp_2020/O3/is.B.x.baseline").get("Mop/s total", 0)
    AUX[3][2] = processFile(
        "TP1/NPB3.3.1/NPB3.3-ICC/results/is/Comp_2020/O3/is.C.x.baseline").get("Mop/s total", 0)

    labels = ("GCC 5.3.0", "GCC 7.2.0", "ICC 2019", "ICC 2020")
    barGrafBuilder(
        AUX, labels, title="Mop\'s for different compiler versions - IS kernel")

cbt()
cft()
cis()

gbt()
gft()
gis()

isOMPCOMP()
ftOMPCOMP()
btOMPComp()
hybrid()
#isOMPCOMPTimes()
#ftOMPCOMPTimes()
#btOMPCompTimes()
#hybridTimes()


