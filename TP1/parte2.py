import matplotlib

import os
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


import re


def iostatRead(path):
    f = open(path,"r")
    text = f.read()
    text = text.splitlines()
    i = 0
    CPU = []
    SDA = []
    for l in text:
        if (i-3)%6 ==0:
            CPU += [l.split()]
        if (i-6)%6 == 0:
            SDA += [l.split()]
        i+=1
    
    #####
    f.close()
    return CPU,SDA

#iostatRead("TP1/NPB3.3.1/NPB3.3-SER/tests/bt.C_iostat.txt")

def vmstat(path):
    f = open(path,"r")
    text = f.read()
    text = text.replace("procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu-----\n","")
    text = text.replace("r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st\n","")
    text = text.splitlines()
    arr = []
    for t in text:
        arr+=[t.split()]
    f.close()
    return arr







def plotXY(x, y, color, lab, title,folder):
    plt.figure()
    plt.plot(y, color, label=lab)
    plt.title(title)
    plt.legend()

    plt.savefig(folder)


def drawMemoryLine(ph,folder):
    t = np.array(vmstat(ph))
    t = t.astype(np.float)
    plotXY(range(0,t.shape[0]), t[:,3], "r", "memory(bytes)", "",folder)

drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\ft.A_vmstat.txt","MEM_ft_A_SER_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\ft.A_vmstat.txt","MEM_ft_A_OMP_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\ft.A_vmstat.txt","MEM_ft_A_MPI_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\ft.B_vmstat.txt","MEM_ft_B_SER_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\ft.B_vmstat.txt","MEM_ft_B_OMP_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\ft.B_vmstat.txt","MEM_ft_B_MPI_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\bt.A_vmstat.txt","MEM_bt_A_SER_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\bt.A_vmstat.txt","MEM_bt_A_OMP_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.A_vmstat.txt","MEM_bt_A_MPI_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\bt.B_vmstat.txt","MEM_bt_B_SER_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\bt.B_vmstat.txt","MEM_bt_B_OMP_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.B_vmstat.txt","MEM_bt_B_MPI_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\bt.C_vmstat.txt","MEM_bt_C_SER_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\bt.C_vmstat.txt","MEM_bt_C_OMP_VMSTAT.png")
drawMemoryLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.C_vmstat.txt","MEM_bt_C_MPI_VMSTAT.png")

def plotXYZ(x, y,y2, color, lab,lab2, title,folder):
    plt.figure()
    plt.plot(y, color, label=lab)
    plt.plot(y2, 'b', label=lab2)
    plt.title(title)
    plt.legend()

    plt.savefig(folder)
    plt.close()

def drawCPULine(pt,folder):
    CPU,SDA = iostatRead(pt)
    CPU = np.array(CPU)
    CPU = CPU.astype(np.float)
    plotXYZ(range(0,CPU.shape[0]), CPU[:,0],CPU[:,5], "r", "user %","idle %","",folder)
    

#drawCPULine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.C_iostat.txt")

drawCPULine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\ft.A_iostat.txt","CPU_ft_A_SER_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\ft.A_iostat.txt","CPU_ft_A_OMP_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\ft.A_iostat.txt","CPU_ft_A_MPI_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\ft.B_iostat.txt","CPU_ft_B_SER_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\ft.B_iostat.txt","CPU_ft_B_OMP_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\ft.B_iostat.txt","CPU_ft_B_MPI_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\bt.A_iostat.txt","CPU_bt_A_SER_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\bt.A_iostat.txt","CPU_bt_A_OMP_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.A_iostat.txt","CPU_bt_A_MPI_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\bt.B_iostat.txt","CPU_bt_B_SER_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\bt.B_iostat.txt","CPU_bt_B_OMP_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.B_iostat.txt","CPU_bt_B_MPI_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-SER\\tests\\bt.C_iostat.txt","CPU_bt_C_SER_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-OMP\\tests\\bt.C_iostat.txt","CPU_bt_C_OMP_IOSTAT.png")
drawCPULine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.C_iostat.txt","CPU_bt_C_MPI_IOSTAT.png")

def drawTPSLine(pt,folder):
    CPU,SDA = iostatRead(pt)
    SDA = np.array(SDA)
    y=[]
    for a in SDA:
        y+=[a[1]]
    y = np.array(y)
    y=y[1:]
    print(y)
    y = y.astype(np.float)
    plotXY(range(0,SDA.shape[0]), y, "r", "TPS","",folder)

drawTPSLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\ft.B_iostat.txt","TPS_ft_B_OMP_IOSTAT.png")
drawTPSLine("TP1\\NPB3.3.1\\NPB3.3-MPI\\tests\\bt.C_iostat.txt","TPS_bt_C_MPI_IOSTAT.png")