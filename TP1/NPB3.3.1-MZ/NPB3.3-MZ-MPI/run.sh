#!/bin/sh

module load gcc/5.3.0
module load gnu/openmpi_eth/1.8.4

mpirun -np 16 ./bin/bt-mz.A.16 > bt-mz.A.16.baseline
mpirun -np 32 ./bin/bt-mz.B.32 > bt-mz.B.32.baseline
mpirun -np 32 ./bin/bt-mz.C.32 > bt-mz.C.32.baseline
