this string flag;

syscall::openat:entry
{
	path = copyinstr(arg1);
    flags = arg2;
    pathstart =  path;
    pathstart[4] = '\0'
}

syscall::openat:return
/pathstart == "/etc"/
{
    this->flag = strjoin(
        flags & O_WRONLY ? "O_WRONLY" : flags & O_RDWR ? "O_RDWR": "O_RDONLY",
            strjoin(
                flags & O_APPEND  ? "|O_APPEND" : "",
                flags & O_CREAT   ? "|O_CREAT" : "")
            );
    printf("\n\tExecutavel: %s\n\tPID: %d UID: %d GID: %d \n\tPath: %s\n\tFlags: %s\n\tReturn: %d",
        execname,
        curpsinfo->pr_pid,
        curpsinfo->pr_uid,
        curpsinfo->pr_gid,
        path,
        this->flag,
        arg0
    );
}


/*

a82885@solaris:~/TP2$ dtrace -s opentracer.d -c "./4.sh"
dtrace: script 'opentracer.d' matched 2 probes
couldn't set locale correctly
tee: /tmp/test: Permission denied
#
# Copyright (c) 1988, 2016, Oracle and/or its affiliates. All rights reserved.
#
# The /etc/inittab file controls the configuration of init(8); for more
# information refer to init(8) and inittab(5).  It is no longer
# necessary to edit inittab(5) directly; administrators should use the
# Solaris Service Management Facility (SMF) to define services instead.
# Refer to smf(7) and the System Administration Guide for more
# information on SMF.
#
# For modifying parameters passed to ttymon, use svccfg(8) to modify
# the SMF repository. For example:
#
#       # svccfg
#       svc:> select system/console-login:default
#       svc:/system/console-login> setprop ttymon/terminal_type = "xterm"
#       svc:/system/console-login> refresh
#       svc:/system/console-login> exit
#
dev::sysinit:/usr/sbin/devfsadm -P
ap::sysinit:/usr/sbin/autopush -f /etc/iu.ap
smf::sysinit:/lib/svc/bin/svc.startd >/dev/msglog 2>&1 </dev/console
p3:s1234:powerfail:/usr/sbin/shutdown -y -i5 -g0 >/dev/msglog 2>&1
dtrace: pid 23189 has exited
 CPU     ID                    FUNCTION:NAME
   5  19738                    openat:return
        Executavel: cat
        PID: 23191 UID: 1025 GID: 5000
        Path: /etc/inittab
        Flags: O_RDONLY
        Return: 3

*/

