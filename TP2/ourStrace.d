syscall:::entry
{
    begin[probefunc] = timestamp;
    @calls[probefunc] = count();
    @totalcalls = count();
}

syscall:::return
{
    error = errno ? 1 : 0;
    time[probefunc] = timestamp - begin[probefunc];
    @errorcount[probefunc] = sum(error);
    @timespent[probefunc] =  sum(time[probefunc]);
    @totalerrors = sum(error);
    @totaltime = sum(timestamp - begin[probefunc]);
}


END
{
    printf("\n%-20s %20s %20s %20s\n","syscall","nanoseconds","calls","errors");
    printf("%-20s %20s %20s %20s\n","-------","-----------","----","-----");
    printa("%-20s %20@u %20@d %20@d\n", @timespent,@calls,@errorcount);
    printf("%-20s %20s %20s %20s\n%-20s ","-------","-----------","----","-----","syscall");
    printa("%20@d %20@d %20@d\n",@totaltime,@totalcalls,@totalerrors);
}




/*
a82467@solaris:~$ truss -c ls
hello.d        local.cshrc    local.login    local.profile  opentracer.d   script.sh
syscall               seconds   calls  errors
_exit                    .000       1
write                    .000       1
close                    .000       2
brk                      .000       5
getpid                   .000       1
ioctl                    .000       3
execve                   .000       1
fstatat                  .000       4
openat                   .000       3       1
getdents                 .000       2
sigfillset               .000       1
getcontext               .000       1
setustack                .000       1
mmap                     .000       2
mmapobj                  .000       1
getrlimit                .000       1
memcntl                  .000       3
sysinfo                  .000       1
lwp_private              .000       1
resolvepath              .000       3
                     --------  ------   ----
sys totals:              .001      38      1
usr time:                .000
elapsed:                 .020
*/