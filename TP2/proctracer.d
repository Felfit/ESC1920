
BEGIN
{
    i = 0;
}

syscall::openat:entry
{
    self->flags = arg2;
    @open_files[curpsinfo->pr_pid, execname] = count();
}

syscall::openat:return
/ self->flags & O_CREAT /
{
    @create_files[curpsinfo->pr_pid, execname] = count();
}

syscall::openat:return
/ errno != 0 /
{
    @success[curpsinfo->pr_pid, execname] = count();
}

profile:::tick-1sec
{
    i = i + 1;
}

profile:::tick-1sec
/ i == $1 /
{
    printf("\n===== Date/Time = %Y ===== \n", walltimestamp);
    printf("PID\t\t\tExecname\tOpen\tCreate\tSuccess\n");
    printa("%d\t %20s\t\t %@d\t %@d\t %@d\n",
        @open_files,
        @create_files,
        @success
    );
    i = 0;
}

